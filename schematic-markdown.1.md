% SCHEMATIC-MARKDOWN(1) Schematic User Manuals | Version 0.3.1
% Evan Hanson <evhan@foldling.org>
% August 2018

# NAME

**schematic-markdown** - Markdown generator for Scheme source

# SYNOPSIS

**schematic-markdown** \[*option* ...]

# DESCRIPTION

**schematic-markdown** generates Markdown from source code read from
standard input, writing to standard output.

# OPTIONS

-c, --comment-prefix
:   Specify an alternative line comment prefix string. By default, lines
    beginning with ";;" and ";;;" are considered comments.

-h, --help
:   Display a usage summary and exit.

-v, --version
:   Print version information to standard output and exit.

# SEE ALSO

`schematic-extract`(1), `schematic-wiki`(1)

The Markdown format is specified by
<http://daringfireball.net/projects/markdown/syntax>.
