# schematic

Tools for Scheme programming.

## Requirements

Any R7RS Scheme and the [optimism][] library.

[optimism]: https://git.foldling.org/optimism.git

## Installation

To install for CHICKEN, run `chicken-install`:

    $ git clone --recursive https://git.foldling.org/schematic.git
    $ cd schematic
    $ chicken-install

Other Schemes can install the Schematic libraries by placing all
directories in `src/` on the system's load path.

## Usage

On CHICKEN, three programs are included: `schematic-format(1)`,
`schematic-markdown(1)` and `schematic-wiki(1)`. Other Schemes can run
these programs as scripts from the project directory, or after
installing the Schematic libraries.

Refer to the manual pages for each program, or `schematic-format.1.md`,
`schematic-wiki.1.md` and `schematic-markdown.1.md`, for usage details.

## Contributing

Please report issues [here](https://todo.sr.ht/~evhan/schematic).

Patches can be mailed to the author.

## Author

Evan Hanson <evhan@foldling.org>

## License

BSD. See LICENSE for details.
