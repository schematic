;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;; Schematic program tests.
;;;
;;; Copyright (c) 2014-2019, Evan Hanson <evhan@foldling.org>
;;; See LICENSE for details.
;;;

(import (chicken format)
        (chicken file posix)
        (chicken process)
        (schematic process))

(define programs
  '("schematic-extract"
    "schematic-format"
    "schematic-markdown"
    "schematic-wiki"))

;; --version flag.
(for-each (lambda (bin)
            (test (format "~a (version)" bin)
                  (string->symbol version)
                  (with-input-from-pipe (format "../~a --version" bin) read)))
          programs)

;; Empty input.
(for-each (lambda (bin)
            (let*-values (((i o) (create-pipe))
                          ((_)   (file-close o))
                          ((pid) (process-fork
                                  (lambda ()
                                    (duplicate-fileno i fileno/stdin)
                                    (process-execute (format "../~a" bin)))))
                          ((_ _ status) (process-wait pid)))
              (test-assert (format "~a (no input)" bin)
                           (zero? status))))
          programs)
