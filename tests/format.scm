;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;; Schematic formatting tests.
;;;
;;; Copyright (c) 2019, Evan Hanson <evhan@foldling.org>
;;; See LICENSE for details.
;;;

(import (chicken sort)
        (chicken file)
        (chicken io)
        (chicken pathname)
        (schematic format))

(define (check input expected)
  (let ((input-port  (open-input-file input))
        (output-port (open-output-string)))
    (format-scheme input-port output-port)
    (test (string-append "format (" (pathname-file input) ")")
          (with-input-from-file expected read-string)
          (get-output-string output-port))))

(for-each
 (lambda (input expected)
   (check input expected))
 (sort (glob "format/*.input") string<?)
 (sort (glob "format/*.expected") string<?))
