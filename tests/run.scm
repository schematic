;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;; Schematic tests.
;;;
;;; These tests requires CHICKEN Scheme.
;;;
;;; See this project's README for more information.
;;;
;;; Copyright (c) 2014-2019, Evan Hanson <evhan@foldling.org>
;;; See LICENSE for details.
;;;

(cond-expand
  (chicken
   (import (test)))
  (else
   (display "Tests require CHICKEN Scheme, skipping.\n")
   (exit 0)))

(include-relative "programs.scm")
(include-relative "format.scm")

(test-exit)
