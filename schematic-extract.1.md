% SCHEMATIC-EXTRACT(1) Schematic User Manuals | Version 0.3.1
% Evan Hanson <evhan@foldling.org>
% August 2018

# NAME

**schematic-extract** - extract commented definitions from Scheme source

# SYNOPSIS

**schematic-extract** \[*option* ...]

# DESCRIPTION

**schematic-extract** generates descriptive s-expressions from commented
definitions in Scheme source.

Code is read from standard input and scanned for commented definition
forms. For each of these, an s-expressive specification of the following
form is written to standard output:

    <specification> = (<comment> (<type> . <form>))
    <comment>       = string?
    <form>          = any?
    <type>          = 'procedure | 'syntax | 'constant
                    | 'parameter | 'record | 'string

# OPTIONS

-c, --comment-prefix
:   Specify an alternative line comment prefix string. By default, lines
    beginning with ";;" and ";;;" are considered comments.

-t, --types
:   Use type declarations to populate svnwiki tags rather than the
    associated `(define ...)` form.

-h, --help
:   Display a usage summary and exit.

-v, --version
:   Print version information to standard output and exit.

# CAVEATS

Only reasonably simple definition styles are recognized, and it's easy
to confuse **schematic-extract** such that valid, commented definitions
are skipped. For example, the common "let over lambda" idiom is
unrecognized, and will be silently ignored.

Any definitions not at the toplevel will be ignored, unless they occur
within a supported library definition form. Currently, the R7RS's
`define-library`, CHICKEN's `module`, and Gauche's `define-module` are
recognized.

# SEE ALSO

`schematic-markdown`(1), `schematic-wiki`(1)
