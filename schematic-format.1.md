% SCHEMATIC-FORMAT(1) Schematic User Manuals | Version 0.3.1
% Evan Hanson <evhan@foldling.org>
% August 2018

# NAME

**schematic-format** - Scheme source formatter

# SYNOPSIS

**schematic-format** \[*option* ...] \[*indent-file*]

# DESCRIPTION

**schematic-format** is a formatter for R7RS Scheme source. It reads
Scheme code from standard input, reindents it according to a fairly
conventional set of rules, and writes the result to standard output.

# OPTIONS

-b
:   Enable bracket parentheses. When enabled, brackets ("[" and "]")
    will be treated as though they were parentheses. This option
    supercedes bracket closure (**-c**).

-c
:   Enable bracket closure. When enabled, any closing brackets ("]") in
    the input will cause all open forms to be closed before continuing.

-t *tabstop*
:   Enable tabbed indentation. When enabled, output will be indented by
    tabs rather than spaces, with each tab being equivalent to *tabstop*
    spaces.

-h, --help
:   Display a usage summary and exit.

-v, --version
:   Print version information to standard output and exit.

# INDENT FILE

An *indent-file*, if given, may specify custom indentation rules. It
should contain a single S-expression, which must be a list of
indentation rules where each rule is one of the following forms:

    (keyword . offset)
    (keyword offset ...)

Each `keyword` is a symbol specifying the head of the source forms for
which the rule should be activated, and each `offset` is an integer
value specifying a (characterwise, columnar) offset from the previous
indentation level to use for that form.

For example, the rule `(receive 1)` would cause the body of each
`receive` form to be indented one space beyond the head of the form:

    (receive 1) => (receive (a b) (foo bar)
                     (baz a)
                     (qux b))

Rules with multiple offsets are applied to the form's data in order,
with the final offset persisting until the form is closed:

    (receive 3 3 1) => (receive
                           (a b)
                           (foo bar)
                         (bar a)
                         (qux b))

Negative offsets are specifically allowed.

# CAVEATS

`cond`'s clauses are indented by one space beyond its opening
parenthesis by default, which may surprise some Emacs users.

Implementation-specific reader extensions such as here documents and
alternative parenthetical syntaxes may not be handled nicely.

# SEE ALSO

`schematic-markdown`(1),
[`scmindent`](http://www.ccs.neu.edu/home/dorai/scmindent/index.html)
