Unreleased
==========
- The documentation extraction procedure now supports `(declare ...)`
  forms, identified by value type `declaration`.

0.3.1
=====
- Fix installation on CHICKEN, which was broken in 0.3.1 due to a
  missing file.

0.3.0
=====
- Port to CHICKEN 5.
- The 'command-line' submodule has been removed. R7RS users must now
  install the new 'optimism' library, while on CHICKEN this is installed
  automatically as an egg dependency.

0.2.1
=====
- A bug in the formatter's handling of multi-offset rules at line
  endings has been fixed. Thanks to vktec for reporting the issue.

0.2.0
=====
- Introduce changelog.
