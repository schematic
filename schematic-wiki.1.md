% SCHEMATIC-WIKI(1) Schematic User Manuals | Version 0.3.1
% Evan Hanson <evhan@foldling.org>
% August 2018

# NAME

**schematic-wiki** - svnwiki generator for Scheme source

# SYNOPSIS

**schematic-wiki** \[*option* ...]

# DESCRIPTION

**schematic-wiki** generates svnwiki documentation fragments from
commented Scheme source. These are suitable for use on the CHICKEN wiki.

Code is read from standard input and scanned for commented definition
forms. For each of these, an svnwiki tag and the comment's contents are
written to standard output.

# OPTIONS

-c, --comment-prefix
:   Specify an alternative line comment prefix string. By default, lines
    beginning with ";;" and ";;;" are considered comments.

-t, --types
:   Use type declarations to populate svnwiki tags rather than the
    associated `(define ...)` form.

-h, --help
:   Display a usage summary and exit.

-v, --version
:   Print version information to standard output and exit.

# CAVEATS

Any limitations of `schematic-extract`(1) apply equally here.

# SEE ALSO

`schematic-extract`(1), `schematic-markdown`(1)

The CHICKEN wiki's svnwiki syntax is documented at
<http://wiki.call-cc.org/edit-help>.
