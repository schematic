;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;; schematic-extract.scm
;;;
;;; Extracts commented definitions from Scheme source.
;;;
;;; See this project's README for more information.
;;;
;;; Copyright (c) 2013-2018, Evan Hanson <evhan@foldling.org>
;;; See LICENSE for details.
;;;

(import (schematic extract)
        (schematic process)
        (scheme base)
        (scheme process-context)
        (scheme write))

(define --comment-prefixes '(";;" ";;;"))
(define --types #false)

(with-exception-handler error-exit
 (lambda ()
   (for-each
    (lambda (option)
      (case (car option)
        ((-h --help)
         (display "Usage: ")
         (display (car (command-line)))
         (display " [-c <string>]")
         (newline)
         (exit))
        ((-v --version)
         (display version)
         (newline)
         (exit))
        ((-c --comment-prefix)
         (set! --comment-prefixes (cdr option)))
        ((-t --types)
         (set! --types #true))
        ((--)
         (if (pair? (cdr option))
             (error "Unrecognized command line argument" (cadr option))
             (extract-definitions
              --comment-prefixes
              --types
              (current-input-port)
              (current-output-port))))))
    (command-line-options
     '(((-c --comment-prefix) string)
       ((-t --types)))))))
