;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;; schematic/format.sld
;;;
;;; See this project's README and schematic/format.scm for more
;;; information.
;;;
;;; Copyright (c) 2013-2018, Evan Hanson <evhan@foldling.org>
;;; See LICENSE for details.
;;;

(define-library (schematic format)
  (import (scheme base)
          (scheme case-lambda)
          (scheme char)
          (scheme read)
          (scheme write))
  (export format-scheme keyword-indent)
  (export bracket-closure? bracket-parentheses?)
  (export tabstop-length)
  (include "format.scm"))
