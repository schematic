;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;; schematic/extract.sld
;;;
;;; See this project's README and schematic/extract.scm for more
;;; information.
;;;
;;; Copyright (c) 2013-2018, Evan Hanson <evhan@foldling.org>
;;; See LICENSE for details.
;;;

(define-library (schematic extract)
  (import (schematic read)
          (scheme base)
          (scheme case-lambda)
          (scheme read)
          (scheme write))
  (export extract-definitions)
  (include "extract.scm"))
