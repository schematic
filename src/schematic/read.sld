;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;; schematic/read.sld
;;;
;;; See this project's README and schematic/read.scm for more
;;; information.
;;;
;;; Copyright (c) 2013-2018, Evan Hanson <evhan@foldling.org>
;;; See LICENSE for details.
;;;

(define-library (schematic read)
  (import (scheme base)
          (scheme case-lambda)
          (scheme char)
          (scheme write))
  (export port-fold-source-sections)
  (include "read.scm"))
