;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;; schematic/process.sld
;;;
;;; See this project's README for more information.
;;;
;;; Copyright (c) 2014-2018, Evan Hanson <evhan@foldling.org>
;;; See LICENSE for details.
;;;

(define-library (schematic process)
  (import (scheme base)
          (scheme write)
          (scheme process-context))
  (export command-line-options error-exit version)
  (cond-expand
    (chicken
     (import (optimism getopt-long)))
    (else
     (import (foldling optimism getopt-long))))
  (begin

    ;; The Schematic version, shared across all programs.
    (define version "0.3.1")

    ;; Options common to all programs.
    (define common-options
      '(((-h --help))
        ((-v --version))))

    ;; Parses `(command-line)` against the given `spec`.
    (define (command-line-options spec)
      (parse-command-line (append spec common-options)))

    ;; Prints an error message for `e` to standard error, then exits
    ;; with an error status.
    (define (error-exit e)
      (let ((message   (error-object-message e))
            (arguments (error-object-irritants e)))
        (when (string? message)
          (parameterize ((current-output-port (current-error-port)))
            (display "Error: ")
            (display message)
            (when (pair? arguments)
              (display ": ")
              (display (car arguments)))
            (newline))
          (exit 1))))))
